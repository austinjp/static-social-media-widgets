<?php
/*
  Plugin Name: Static Social Media Widgets
  Plugin URI: https://bitbucket.org/austinjp/static-social-media-widgets
  Description: Share buttons that don't invade your privacy, and render quickly.
  Version: 0.0.2
  Author: Austin Plunkett
  Author URI: https://bitbucket.org/austinjp
*/

/**
 * Register Widget
 */
function static_social_media_widget_init() {
    register_widget( 'StaticSocialMediaShareWidget' );
    register_widget( 'StaticSocialMediaFollowWidget' );
}
add_action( 'widgets_init', 'static_social_media_widget_init' );


class StaticSocialMediaFollowWidget extends StaticSocialMediaShare {
    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {

	load_plugin_textdomain('static_social_media_widget_follow', false);

	parent::__construct(
	    'static_social_media_widget_follow',
	    'Static social media follow buttons',
	    array(
		'description' => __(
		    'Static social media follow buttons. Fast. Privacy-respecting.',
		    'static_social_media_widget_follow'
		)
	    )
	);
    }

    function widget($args, $instance) { }
    function update($new_instance, $old_instance) { }
    function form($instance) {
        $defaults = self::_get_default_widget_settings();
        $instance = wp_parse_args( (array) $instance, $defaults );
	?><p>...</p><?php
    }
}

class StaticSocialMediaShareWidget extends StaticSocialMediaShare {
    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {

	load_plugin_textdomain('static_social_media_widget_share', false);

	parent::__construct(
	    'static_social_media_widget_share',
	    'Static social media sharing buttons',
	    array(
		'description' => __(
		    'Static social media sharing buttons. Fast. Privacy-respecting.',
		    'static_social_media_widget_share'
		)
	    )
	);
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array, An array of standard parameters for widgets in this theme
     * @param array, An array of settings for this widget instance
     * @return string, Echo output
     **/
    function widget($args, $instance) {

	wp_enqueue_style( 'static-social-media-widgets', plugins_url('style.min.css', __FILE__) );

	if (!isset($args['widget_id'])) { $args['widget_id'] = null; }
        extract($args);
	/*
          $title = apply_filters( 'widget_title', $instance['title'] );
          $url = $instance['url'];
          $pagetitle = $instance['pagetitle'];
	*/

	echo $before_widget;

	echo $before_title;
	if (isset($instance['name'])) { /* FIXME Tidy this up */
	    $widget_title = esc_attr($instance['name']);
	    if( empty( $widget_title ) ) {
		echo self::_get_default_widget_settings()['name'];
	    }
	    else {
		echo $widget_title;
	    }
	} else {
	    echo self::_get_default_widget_settings()['name'];
	}
	echo $after_title;

	echo '<ul class="ssmb">' . "\n";
	foreach (self::_get_providers_keys_as_array() as $p) {
	    $b = self::_render_button($p, $args, $instance);
	    if ($b) {
		echo '<li class="ssmb-b ssmb-b-' . $p . '">' . $b . '</li>' . "\n";
	    } else {
		echo '<li>' . $p . '</li>' . "\n";
	    }
	}
	echo '</ul>' . "\n";

	echo $after_widget;
    }


    /**
     * ...
     *
     * @return string, Echo output
     */
    function _render_button($provider = null, $args = null, $instance = null) {
	if ($provider == null) { return null; }

	$prov = self::_get_providers();

	return '<a class="ssmb-a ssmb-a-' . $provider . '" href="'
	    . (isset($prov[$provider]['url']) ? $prov[$provider]['url'] : '#')
	    . '" title="Share'
	    . (isset($prov[$provider]['provider']) ? ($prov[$provider]['provider'] == 'Email' ? ' by email' : ' on ' . $prov[$provider]['provider']) : '')
	    . '"'
	    . ($prov[$provider]['provider'] == 'Email' ? '' : ' target="_blank"')
	    . ($prov[$provider]['provider'] == 'WhatsApp' ? ' data-action="share/whatsapp/share"' : '')
	    . '>'
	    . '<span class="ssmb-icon ssmb-icon-' . $provider . '"></span>'
	    . '<span class="ssmb-provider">'
	    . (isset($prov[$provider]['provider']) ? $prov[$provider]['provider'] : 'Share')
	    . '</span></a>'
	    . '<span class="ssmb-count"></span>'
	    . "\n";
    }

    /**
     * Sanitizes form inputs on save
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array $new_instance
     */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	if (isset($new_instance['name'])) { $instance['name'] = esc_attr( $new_instance['name'] ); }
	if (isset($new_instance['url'])) { $instance['url'] = esc_attr( $new_instance['url'] ); }
	if (isset($new_instance['text'])) { $instance['text'] = esc_attr( $new_instance['text'] ); }
	if (isset($new_instance['summary'])) { $instance['summary'] = esc_attr( $new_instance['summary'] ); }
	return $instance;
    }

    /**
     * Build the widget's form
     *
     * @param array $instance, An array of settings for this widget instance
     * @return null
     */
    function form( $instance ) {
	/* Set up some default widget settings. */
        $defaults = self::_get_default_widget_settings();
        $instance = wp_parse_args( (array) $instance, $defaults );
	?>
<p>
  <label for="<?php echo $this->get_field_id( 'name' ); ?>"><?php _e( 'Title to display above the widget. Defaults to "' . $defaults['name'] . '".', 'static-social-media-widgets' );?></label>
  <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" value="<?php echo $instance['name']; ?>">
</p>

<p>
  <label for="<?php echo $this->get_field_id( 'url' ); ?>"><?php _e( 'URL. Defaults to the URL of the current page. If you set this then all shares will link back to this single URL.', 'static-social-media-widgets' );?></label>
  <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" value="<?php echo $instance['url']; ?>">
</p>

<p>
  <label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text to share. Defaults to the title of the shared page. If you set this then all shares will use this text.', 'static-social-media-widgets' );?></label>
  <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" value="<?php echo $instance['text']; ?>">
</p>

<!--
<p>
  What to display:<br>
  <input class="widefat" type="radio" id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" value="icons=1&text=0" <?php if(true) { echo ' checked'; } ?>>
  <label for="<?php echo $this->get_field_id( 'display' ); ?>"><?php _e( 'Icons only', 'static-social-media-widgets' );?></label>
  <br>
  <input class="widefat" type="radio" id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" value="icons=0&text=1" <?php if(false) { echo ' checked'; } ?>>
  <label for="<?php echo $this->get_field_id( 'display' ); ?>"><?php _e( 'Text only', 'static-social-media-widgets' );?></label>
  <br>
  <input class="widefat" type="radio" id="<?php echo $this->get_field_id( 'display' ); ?>" name="<?php echo $this->get_field_name( 'display' ); ?>" value="icons=1&text=1" <?php if(false) { echo ' checked'; } ?>>
  <label for="<?php echo $this->get_field_id( 'display' ); ?>"><?php _e( 'Icons and text', 'static-social-media-widgets' );?></label>
  <br>
  <input class="widefat" type="checkbox" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" value="count=1" <?php if(true) { echo ' checked'; } ?>>
  <label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Display count (applies to all buttons)', 'static-social-media-widgets' );?></label>
</p>
-->
        <?php
    }
}


class StaticSocialMediaShare extends WP_Widget {
    /**
     * Constants and helper functions
     */
    static function _get_default_widget_settings() {
	return array(
	    'name' => 'Share',
	    'url' => null,
	    'text' => null,
	    'summary' => null
	);
    }

    function _get_providers() {
	return array (
	    'facebook' => array( 'provider' => 'Facebook', 'url' => 'https://www.facebook.com/sharer/sharer.php?u=' . self::_get_share_url()),
	    'googleplus' => array( 'provider' => 'Google+', 'url' => 'https://plus.google.com/share?url=' . self::_get_share_url()),
	    'linkedin' => array( 'provider' => 'LinkedIn', 'url' => 'https://www.linkedin.com/shareArticle?mini=true&url=' . self::_get_share_url() . '&title=' . self::_get_share_title() . '&summary=' . self::_get_share_summary() . '&source=' . self::_get_share_url()),
	    'reddit' => array( 'provider' => 'Reddit', 'url' => 'https://reddit.com/submit?url=' . self::_get_share_url() . '&title=' . self::_get_share_title()),
	    'twitter' => array( 'provider' => 'Twitter', 'url' => 'https://twitter.com/intent/tweet?source=' . self::_get_share_url() . '&text=' . self::_get_share_title() . ' ' . self::_get_share_url()),
	    'whatsapp' => array( 'provider' => 'WhatsApp', 'url' => 'whatsapp://send?text=' . self::_get_share_summary()),
	    'email' => array( 'provider' => 'Email', 'url' => 'mailto:?subject=' . self::_get_share_title() . '&body=' . self::_get_share_url())
	);
    }

    function _get_providers_keys_as_array() {
	return array_keys(self::_get_providers());
    }

    function _get_share_title() {
	/* FIXME Return default title if necessary */
	return get_the_title();
    }

    function _get_share_url() {
	/* FIXME Return default link if necessary */
	global $wp;
	return urlencode_deep(home_url(add_query_arg(array(),$wp->request)));
    }

    /*
    private function _get_the_excerpt_maxwords($len = 20) {
	if (null === $len || $len <=0 ) { return get_the_excerpt(); }
	$excerpt = get_the_excerpt();
	return $excerpt;
    }
    */

    function _get_share_summary() {
	return urlencode_deep(get_the_excerpt());
    }

    /**
     * ...
     *
     * @return string, Echo output
     */
    function static_social_media_widget() {
    }

}
