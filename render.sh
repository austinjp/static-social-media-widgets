#/bin/bash
java -jar /usr/local/lib/yuicompressor-2.4.8.jar style.css > style.min.css
cat /dev/null > icons.min.css
for i in `ls -1 *-inline.svg `; do
id=`echo -n $i | sed s/-inline.svg//`
echo -n span.ssmb-icon-$id{background-image:url\(\"$(cat $i)\"\)\} >> icons.min.css
done
cat icons.min.css >> style.min.css
