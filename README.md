# Static social media widgets

Wordpress plugin that provides social media buttons. The buttons are static, meaning that there is no JavaScript and no requests are sent to the servers of Twitter, Facebook etc from the browser of anybody viewing your page. This is great for speed, and for privacy.
